# Script qui extrait le nombre de population des communes du Havre Seine Métropole

# charge la bibliothèque
import pandas as pd

# Lecture du nombre de population par commune téléchargé sur https://statistiques-locales.insee.fr/#view=map1&c=indicator
df=pd.read_csv('../data/pop_communes.csv', sep=';', header=2, dtype=str)

# Filtre des communes limité à LHSM
df=df.loc[df['Code'].isin(['76014', '76017', '76064', '76079', '76117', '76167', '76169', '76196', '76206', '76238', '76239', '76250', '76254', '76268', '76270', '76275', '76296', '76303', '76305', '76307', '76314', '76341', '76351', '76357', '76361', '76404', '76409', '76447', '76477', '76481', '76489', '76501', '76508', '76522', '76533', '76534', '76551', '76552', '76563', '76586', '76595', '76596', '76609', '76615', '76616', '76647', '76657', '76658', '76660', '76693', '76714', '76716', '76734', '76741'])]

# Enregistrement des résultats au format csv dans le dossier data
df.to_csv(r'../data/pop_LHSM.csv', index=False)