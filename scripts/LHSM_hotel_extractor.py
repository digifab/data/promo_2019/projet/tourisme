# -*-coding:UTF-8 -*
# Script qui extrait les hôtels dans les communes du Havre Seine Métropole

# charge la bibliothèque
import pandas as pd

# Lecture des hotels téléchargé sur https://data.classement.atout-france.fr/recherche-etablissements
df=pd.read_csv('../data/etablissements_SM.csv', sep=';', encoding='utf-8', dtype=str)

# Filtre des codes postaux limité à LHSM
df=df.loc[df['CODE POSTAL'].isin(['76610','76600','76620','76280','76790','76930','76430','76133','76290','76700','76310'])]

# Enregistrement des résultats au format csv dans le dossier data
df.to_csv(r'../data/etablissements_LHSM2.csv', index=False)